package com.atlassian.chatmessenger.model;

import com.atlassian.chatmessenger.util.Logger;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Content {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> mentions;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> emoticons;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Link> links;

    private Content(ContentBuilder contentBuilder) {
        this.mentions = contentBuilder.mentions;
        this.emoticons = contentBuilder.emoticons;
        this.links = contentBuilder.links;
    }

    /**
     * Builder class to build the content.
     */
    public static class ContentBuilder {
        private List<String> mentions = new ArrayList<>();
        private List<String> emoticons = new ArrayList<>();
        private List<Link> links = new ArrayList<>();

        public ContentBuilder addMentions(List<String> mentions) {
            if (mentions == null || mentions.isEmpty()) {
                return this;
            }
            this.mentions = mentions;
            return this;
        }

        public ContentBuilder addEmoticons(List<String> emoticons) {
            if (emoticons == null || emoticons.isEmpty()) {
                return this;
            }
            this.emoticons = emoticons;
            return this;
        }

        public ContentBuilder addLinks(List<String> urls) {
            if (urls == null || urls.isEmpty()) {
                return this;
            }
            for (String url : urls) {
                try {
                    URL linkUrl = new URL(url);
                    linkUrl.getAuthority();

                    Link link = new Link(url);
                    link.setTitle(linkUrl.getAuthority());
                    this.links.add(link);
                } catch (MalformedURLException e) {
                    Logger.e("Exception occurred while submitting url");
                }
            }
            return this;
        }

        public Content build() {
            return new Content(this);
        }
    }

}
