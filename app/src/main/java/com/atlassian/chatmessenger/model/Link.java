package com.atlassian.chatmessenger.model;

/**
 * Created by Harsh on 2/20/17.
 */
public class Link {
  private String url;
  private String title;

  public Link(String url) {
    this.url = url;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
