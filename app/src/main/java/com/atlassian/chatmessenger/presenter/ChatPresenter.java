package com.atlassian.chatmessenger.presenter;

import com.atlassian.chatmessenger.service.ChatRepository;
import com.atlassian.chatmessenger.ui.ChatViewActions;

/**
 * Presenter class to route the calls from view to the service.
 */
public class ChatPresenter {
  private final ChatRepository chatRepository;
  private final ChatViewActions chatViewActions;

  /**
   * @param chatRepository  : repository
   * @param chatViewActions : actions to be called in case a UI operation is required.
   */
  public ChatPresenter(ChatRepository chatRepository, ChatViewActions chatViewActions) {
    this.chatRepository = chatRepository;
    this.chatViewActions = chatViewActions;
  }

  /**
   * @param chatMessage : chat message received from the user.
   */
  public void sendButtonTapped(String chatMessage) {
    if (isMessageValid(chatMessage)) {
      String contentJson = chatRepository.getContentJson(chatMessage);
      chatViewActions.onContentJsonReceived(contentJson);
    } else {
      chatViewActions.onChatMessageValidationFailure();
    }
  }

  /**
   * Verify if the message is valid
   *
   * @param chatMessage : chat message
   * @return : TRUE if the message is valid, FALSE otherwise.
   */
  public boolean isMessageValid(String chatMessage) {
    return chatMessage != null && !chatMessage.isEmpty();
  }
}
