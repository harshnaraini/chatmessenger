package com.atlassian.chatmessenger.service;

import android.support.annotation.NonNull;

import com.atlassian.chatmessenger.model.Content;
import com.atlassian.chatmessenger.model.Link;
import com.atlassian.chatmessenger.service.matcher.EmoticonMatcher;
import com.atlassian.chatmessenger.service.matcher.LinkMatcher;
import com.atlassian.chatmessenger.service.matcher.MentionMatcher;
import com.atlassian.chatmessenger.util.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Repository for doing the service operations on the chat message.
 */
public class ChatRepository {

    /**
     * Get Json from the chat message.
     *
     * @param chatMessage : chat message
     * @return : json
     */
    public String getContentJson(String chatMessage) {
        String contentJson = "";
        Content content = getContent(getWords(chatMessage));
        if (content != null) {
            contentJson = ModelJsonConverter.modelToJson(content);
        }
        return contentJson;
    }

    @NonNull
    private List<String> getWords(String chatMessage) {
        return Arrays.asList(chatMessage.split(" "));
    }

    private Content getContent(List<String> words) {
        Content.ContentBuilder contentBuilder = new Content.ContentBuilder();

        filterMentions(words, contentBuilder);
        filterEmoticons(words, contentBuilder);
        filterLinks(words, contentBuilder);

        return contentBuilder.build();
    }

    private void filterLinks(List<String> words, Content.ContentBuilder contentBuilder) {
        LinkMatcher linkMatcher = new LinkMatcher();
        contentBuilder.addLinks(linkMatcher.matches(words));
    }

    private void filterEmoticons(List<String> words, Content.ContentBuilder contentBuilder) {
        EmoticonMatcher emoticonMatcher = new EmoticonMatcher();
        contentBuilder.addEmoticons(emoticonMatcher.matches(words));
    }

    private void filterMentions(List<String> words, Content.ContentBuilder contentBuilder) {
        MentionMatcher mentionMatcher = new MentionMatcher();
        contentBuilder.addMentions(mentionMatcher.matches(words));
    }
}
