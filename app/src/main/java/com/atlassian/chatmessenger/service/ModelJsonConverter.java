package com.atlassian.chatmessenger.service;

import com.atlassian.chatmessenger.model.Content;
import com.atlassian.chatmessenger.util.Logger;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Model to Json converter
 */
public class ModelJsonConverter {
    /**
     * Convert content model to Json
     *
     * @param content : content
     * @return : Json string
     */
    public static String modelToJson(Content content) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String json = "";
        try {
            json = objectMapper.writeValueAsString(content);
        } catch (JsonProcessingException e) {
            Logger.e(e.getMessage());
        }
        return json;
    }
}
