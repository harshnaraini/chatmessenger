package com.atlassian.chatmessenger.service.matcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Matcher class to detect the words which contain emoticons.
 */
public class EmoticonMatcher implements Matcher {
  private final String EMOTICON_MATCHER_ONLY_ALPHANUMERIC = "^([a-zA-Z0-9]*)$";

  @Override
  public List<String> matches(List<String> words) {
    List<String> emoticons = new ArrayList<>();
    for (String emoticon : words) {
      if (isValidEmoticon(emoticon)) {
        emoticons.add(getWordFromSpecialContent(emoticon));
      }
    }
    return emoticons;
  }

  @Override
  public String getWordFromSpecialContent(String word) {
    return word.substring(1, word.length() - 1);
  }

  private boolean isValidEmoticon(String emoticon) {
    return emoticon.startsWith("(") && emoticon.endsWith(")") && validateEmoticonString(emoticon);
  }


  private boolean validateEmoticonString(String word) {
    String wordWithoutParenthesis = getWordFromSpecialContent(word);
    int wordWithoutParenthesisLength = wordWithoutParenthesis.length();

    return wordWithoutParenthesis.matches(EMOTICON_MATCHER_ONLY_ALPHANUMERIC) && wordWithoutParenthesisLength > 1 && wordWithoutParenthesisLength < 16;
  }
}
