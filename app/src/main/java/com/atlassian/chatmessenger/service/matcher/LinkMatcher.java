package com.atlassian.chatmessenger.service.matcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Matcher class to detect the words which contain links.
 */
public class LinkMatcher implements Matcher {

  @Override
  public List<String> matches(List<String> words) {

    List<String> links = new ArrayList<>();
    for (String link : words) {
      if (isValidUrl(link)) {
        links.add(link);
      }
    }
    return links;
  }

  @Override
  public String getWordFromSpecialContent(String word) {
    return word;
  }

  private boolean isValidUrl(String link) {
    return android.util.Patterns.WEB_URL.matcher(link).matches();
  }
}
