package com.atlassian.chatmessenger.service.matcher;

import java.util.List;

/**
 * Created by Harsh on 2/20/17.
 */

public interface Matcher {
  /**
   * Return the list of strings which match the pattern without including the delimiters
   *
   * @return
   */
  List<String> matches(List<String> words);

  /**
   * Get the word after removing the special content(delimiters) from the word
   *
   * @param word : word as entered
   * @return : word without the delimiters
   */
  String getWordFromSpecialContent(String word);
}
