package com.atlassian.chatmessenger.service.matcher;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Matcher class to detect the words which contain mentions.
 */
public class MentionMatcher implements Matcher {

  @Override
  public List<String> matches(List<String> words) {
    List<String> mentions = new ArrayList<>();
    for (String mention : words) {
      if (isMention(mention)) {
        mentions.add(getWordFromSpecialContent(mention));
      }
    }
    return mentions;
  }

  @Override
  public String getWordFromSpecialContent(String word) {
    return word.substring(1, word.length() - 1);
  }


  private boolean isMention(String mention) {
    return mention.startsWith("@") && mention.length() > 2 && isSpecialCharacter(mention.charAt(mention.length() - 1));
  }

  private boolean isSpecialCharacter(Character character) {
    Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
    java.util.regex.Matcher matcher = p.matcher(Character.toString(character));
    return matcher.find();
  }
}
