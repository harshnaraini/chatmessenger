package com.atlassian.chatmessenger.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.atlassian.chatmessenger.R;
import com.atlassian.chatmessenger.presenter.ChatPresenter;
import com.atlassian.chatmessenger.service.ChatRepository;

public class ChatActivity extends AppCompatActivity implements ChatViewActions {
    private ImageButton sendButton;
    private EditText chatMessage;
    private ChatPresenter chatPresenter;
    private ChatRepository chatRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        chatRepository = new ChatRepository();
        chatPresenter = new ChatPresenter(chatRepository, this);

        initUiFields();
    }

    private void initUiFields() {
        sendButton = (ImageButton) findViewById(R.id.sendMessage);
        chatMessage = (EditText) findViewById(R.id.chatMessageField);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatPresenter.sendButtonTapped(chatMessage.getText().toString());
            }
        });
    }

    @Override
    public void onChatMessageValidationFailure() {
        chatMessage.setError(getString(R.string.message_validation_failure_error));
    }

    @Override
    public void onContentJsonReceived(String contentJson) {
        showJsonDialog(contentJson);
    }

    private void showJsonDialog(String contentJson) {
        new AlertDialog.Builder(this)
                .setTitle("JSON received")
                .setMessage(contentJson)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
