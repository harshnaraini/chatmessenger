package com.atlassian.chatmessenger.ui;

/**
 * Created by Harsh on 2/20/17.
 */

public interface ChatViewActions {
  /**
   * Called when the chat message validation fails.
   */
  void onChatMessageValidationFailure();

  /**
   * ON receiving the content json for the message
   *
   * @param contentJson
   */
  void onContentJsonReceived(String contentJson);

}
