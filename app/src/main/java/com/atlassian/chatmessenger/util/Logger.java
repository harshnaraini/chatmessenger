package com.atlassian.chatmessenger.util;

import android.util.Log;

/**
 * Logger class for general purpose logging.
 */
public final class Logger {
    private static final String TAG = "ChatMessenger";
    private static boolean DEBUG_ENABLED = true;

    public static void d(String msg) {
        if (DEBUG_ENABLED) {
            Log.d(TAG, msg);
        }
    }

    public static void e(String msg) {
        if (DEBUG_ENABLED) {
            Log.e(TAG, msg);
        }
    }

}
