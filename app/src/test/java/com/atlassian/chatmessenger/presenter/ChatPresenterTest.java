package com.atlassian.chatmessenger.presenter;

import com.atlassian.chatmessenger.service.ChatRepository;
import com.atlassian.chatmessenger.ui.ChatViewActions;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ChatPresenterTest {
    @Mock
    private ChatRepository chatRepository;
    @Mock
    private ChatViewActions chatViewActions;

    private ChatPresenter chatPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        chatPresenter = new ChatPresenter(chatRepository, chatViewActions);
    }

    @Test
    public void shouldSendJsonIfChatMessageIsValid() throws Exception {
        doNothing().when(chatViewActions).onChatMessageValidationFailure();
        doNothing().when(chatViewActions).onContentJsonReceived(anyString());
        String expectedJSONString = "JSON string";
        when(chatRepository.getContentJson(anyString())).thenReturn(expectedJSONString);
        chatPresenter.sendButtonTapped("chat Message");
        verify(chatViewActions).onContentJsonReceived(expectedJSONString);

    }


    @Test
    public void shouldReturnValidIfMessageIsValid() throws Exception {
        doNothing().when(chatViewActions).onChatMessageValidationFailure();
        assertTrue(chatPresenter.isMessageValid("Chat Message"));
    }

    @Test
    public void shouldReturnInValidIfMessageIsEmpty() throws Exception {
        assertFalse(chatPresenter.isMessageValid(""));
    }

    @Test
    public void shouldReturnInValidIfMessageIsNull() throws Exception {
        assertFalse(chatPresenter.isMessageValid(null));
    }
}