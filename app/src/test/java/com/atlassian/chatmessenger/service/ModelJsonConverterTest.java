package com.atlassian.chatmessenger.service;

import com.atlassian.chatmessenger.model.Content;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ModelJsonConverterTest {
    @Test
    public void shouldReturnProperJsonIfAllFieldsArePresentInContent() throws Exception {
        Content.ContentBuilder contentBuilder = new Content.ContentBuilder();
        ArrayList<String> emoticons = new ArrayList<>();
        emoticons.add("Hellow123");
        ArrayList<String> mentions = new ArrayList<>();
        mentions.add("Jason");
        ArrayList<String> urls = new ArrayList<>();
        urls.add("https://www.google.com");
        contentBuilder.addEmoticons(emoticons);
        contentBuilder.addMentions(mentions);
        contentBuilder.addLinks(urls);

        String expectedJson = "{\"mentions\":[\"Jason\"],\"emoticons\":[\"Hellow123\"],\"links\":[{\"url\":\"https://www.google.com\",\"title\":\"www.google.com\"}]}";
        assertEquals(ModelJsonConverter.modelToJson(contentBuilder.build()), expectedJson);

    }

    @Test
    public void shouldReturnOnlyMentionsIfEmoticonsAndUrlsAbsent() throws Exception {
        Content.ContentBuilder contentBuilder = new Content.ContentBuilder();
        ArrayList<String> mentions = new ArrayList<>();
        mentions.add("Jason");
        contentBuilder.addMentions(mentions);

        String expectedJson = "{\"mentions\":[\"Jason\"]}";
        assertEquals(ModelJsonConverter.modelToJson(contentBuilder.build()), expectedJson);

    }

    @Test
    public void shouldReturnOnlyMentionsAndEmoticonsIfUrlsAbsent() throws Exception {
        Content.ContentBuilder contentBuilder = new Content.ContentBuilder();
        ArrayList<String> mentions = new ArrayList<>();
        mentions.add("Jason");
        contentBuilder.addMentions(mentions);

        ArrayList<String> emoticons = new ArrayList<>();
        emoticons.add("Hellow123");
        contentBuilder.addEmoticons(emoticons);

        String expectedJson = "{\"mentions\":[\"Jason\"],\"emoticons\":[\"Hellow123\"]}";
        assertEquals(ModelJsonConverter.modelToJson(contentBuilder.build()), expectedJson);
    }

    @Test
    public void shouldReturnEmptyIfNoSpecialContent() throws Exception {
        Content.ContentBuilder contentBuilder = new Content.ContentBuilder();

        String expectedJson = "{}";
        assertEquals(ModelJsonConverter.modelToJson(contentBuilder.build()), expectedJson);
    }

}