package com.atlassian.chatmessenger.service.matcher;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Harsh on 2/21/17.
 */
public class EmoticonMatcherTest {
  private EmoticonMatcher emoticonMatcher;

  @Before
  public void setUp() {
    emoticonMatcher = new EmoticonMatcher();
  }

  @Test
  public void shouldReturnMatchedEmoticonWhenValidationPasses() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("(Hello123)");
    assertTrue(emoticonMatcher.matches(words).contains("Hello123"));
  }

  @Test
  public void shouldNotReturnMatchedEmoticonWhenWordContainsSpecialCharacter() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("(Hello123!2)");
    assertFalse(emoticonMatcher.matches(words).contains("(Hello123)"));
  }

  @Test
  public void shouldNotReturnMatchedEmoticonWhenWordLengthMoreThan15() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("(Hello1234567891012)");
    assertFalse(emoticonMatcher.matches(words).contains("(Hello1234567891012)"));
  }

  @Test
  public void shouldNotReturnMatchedEmoticonWhenWordDoesNotEndsWithParenthesis() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("(Hello123");
    assertFalse(emoticonMatcher.matches(words).contains("(Hello123"));
  }

  @Test
  public void shouldNotReturnMatchedEmoticonWhenWordDoesNotStartWithParenthesis() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("Hello1)");
    assertFalse(emoticonMatcher.matches(words).contains("Hello1)"));
  }
}