package com.atlassian.chatmessenger.service.matcher;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Harsh on 2/21/17.
 */
public class LinkMatcherTest {

  private LinkMatcher linkMatcher;

  @Before
  public void setUp() {
    linkMatcher = new LinkMatcher();
  }

  @Test
  public void shouldReturnValidMatchIfValidationSuccessful() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("https://www.google.com");
    assertTrue(linkMatcher.matches(words).contains("https://www.google.com"));
  }

  @Test
  public void shouldNotReturnMatchedIfUrlInvalid() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("htt://www.google.com");
    assertFalse(linkMatcher.matches(words).contains("htt://www.google.com"));
  }
}