package com.atlassian.chatmessenger.service.matcher;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Harsh on 2/21/17.
 */
public class MentionMatcherTest {

  private MentionMatcher mentionMatcher;

  @Before
  public void setUp() {
    mentionMatcher = new MentionMatcher();
  }

  @Test
  public void shouldReturnValidMatchIfValidationSuccessful() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("@Jason,");
    assertTrue(mentionMatcher.matches(words).contains("Jason"));
  }

  @Test
  public void shouldNotReturnMatchedIfWordDoesNotStartWithAtTheRateSymbol() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("Jason,");
    assertFalse(mentionMatcher.matches(words).contains("Jason,"));
  }

  @Test
  public void shouldNotReturnMatchedIfLengthOfWordIsLessThan1() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("@,");
    assertFalse(mentionMatcher.matches(words).contains("@,"));
  }

  @Test
  public void shouldNotReturnMatchedIfWordDoesNotEndWithSpecialCharacter() throws Exception {
    List<String> words = new ArrayList<>();
    words.add("@Jason");
    assertFalse(mentionMatcher.matches(words).contains("@Jason"));

  }
}